import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import React from 'react';

import App from './pages/app';
import Login from './pages/login';
import Home from './pages/home';
import Detail from './pages/detail';
import NoMeach from './pages/404'
export default function IRouter() {
    return <Router>
        <Switch>
            {/* exact 精准匹配 */}
            <Route exact path="/" component={App}></Route>
            <Route path="/login" component={Login}>
                {/* 路由重定向 */}
                <Redirect to='/home'></Redirect>
            </Route>
            <Route path="/home" component={Home}></Route>
            {/* 动态路由 ：goodid */}
            <Route path="/detail/:goodid " component={Detail}></Route>

            <Route path='*' component={NoMeach}></Route>
        </Switch>
    </Router>
}