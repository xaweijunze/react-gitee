import React,{useState} from 'react';
import { Link } from 'react-router-dom'
import { Button } from 'antd'
export default function App() {
    const [count,setCount] = useState(10);
    return <div className="container">
        <h1> 欢迎来到app</h1>
        <Link to="/login">跳转到登录页面 </Link> <br/>
        <Link to="/login">跳转到home页面 </Link>
        <p>Count:{count}</p>
        <Button onClick ={()=>{setCount(count+1)}}>更新视图</Button>
    </div>
}
// export default class App extends React.Component{

//     handleJump=()=>{
//         this.props.history.push("/login");
//     }

//     render() {
//         return <div className="container">
//             <h1>欢迎来到React的世界</h1>
//             <Link to="/login">点击跳转到登录页</Link>
//             <br/>
//             <Link to="/home">点击跳转到主页面</Link>
//             <br/>
//             <Button onClick={this.handleJump}>登录跳转登录</Button>
//         </div>
//     }
// }